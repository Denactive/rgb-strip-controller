// musicSoundShow / autoLowPass
void musicSoundShow(void) {
  
      RsoundLevel = 0;
      LsoundLevel = 0;

      // первые два режима - громкость (VU meter)
        for (byte i = 0; i < 100; i ++) {                                 // делаем 100 измерений
          RcurrentLevel = analogRead(SOUND_R);                            // с правого
          if (!MONO) LcurrentLevel = analogRead(SOUND_L);                 // и левого каналов     
          if (RsoundLevel < RcurrentLevel) RsoundLevel = RcurrentLevel;   // ищем максимальное
          if (!MONO) if (LsoundLevel < LcurrentLevel) LsoundLevel = LcurrentLevel;   // ищем максимальное
        }
        #if (DEV_LOG == 1) 
          //Serial.print(F("voltage(mV): ")); Serial.print(RsoundLevel/1024*1100); Serial.print(' '); Serial.println(LsoundLevel);
          Serial.print(F("analog data: ")); Serial.print(RsoundLevel); Serial.print(' '); Serial.println(LsoundLevel);
        #endif 
        
        // фильтруем по нижнему порогу шумов
        RsoundLevel = map(RsoundLevel, LOW_PASS, 1023, 0, 500);
        if (!MONO)LsoundLevel = map(LsoundLevel, LOW_PASS, 1023, 0, 500);
        
        // ограничиваем диапазон
        RsoundLevel = constrain(RsoundLevel, 0, 500);
        if (!MONO)LsoundLevel = constrain(LsoundLevel, 0, 500);

        #if (DEV_LOG == 1) 
          Serial.print(F("LP...1023: ")); Serial.print(RsoundLevel); Serial.print(' '); Serial.println(LsoundLevel);
          #endif 
          
        // возводим в степень (для большей чёткости работы)
        RsoundLevel = pow(RsoundLevel, EXP);
        if (!MONO)LsoundLevel = pow(LsoundLevel, EXP);

        // фильтр
        RsoundLevel_f = RsoundLevel * SMOOTH + RsoundLevel_f * (1 - SMOOTH);
        if (!MONO)LsoundLevel_f = LsoundLevel * SMOOTH + LsoundLevel_f * (1 - SMOOTH);

        if (MONO) LsoundLevel_f = RsoundLevel_f;  // если моно, то левый = правому

        // заливаем "подложку"
        if (turn)
          for (byte i = 0; i < NUMPIXELS; i++) 
            strip.setPixelColor(i, backgroundR, backgroundG, backgroundB); 
        else
          for (byte i = 0; i < NUMPIXELS; i++) 
            strip.setPixelColor(i, 0, 0, 0);
        strip.show();
        
        // Для поиска технических неисправностей  
        #if (DEV_LOG == 1) 
        //Serial.print(F('Filtered ')); Serial.print(RsoundLevel_f); Serial.print(' '); Serial.println(LsoundLevel_f);
        #endif
        
        // если значение выше порога - начинаем самое интересное
        if (RsoundLevel_f > minLevel && LsoundLevel_f > minLevel) {

            // расчёт общей средней громкости с обоих каналов, фильтрация.
            // Фильтр очень медленный, сделано специально для автогромкости
            averageLevel = (float)(RsoundLevel_f + LsoundLevel_f) / 2 * averK + averageLevel * (1 - averK);
  
            // принимаем максимальную громкость шкалы как среднюю, умноженную на некоторый коэффициент MAX_COEF
            maxLevel = (float)averageLevel * MAX_COEF;
  
            // преобразуем сигнал в длину ленты 
            Rlenght = map(RsoundLevel_f, 0, maxLevel, 0, segLen); // segLen - длина сегмента NUMPIXELS/2/set.numwaves
            Llenght = map(LsoundLevel_f, 0, maxLevel, 0, segLen);
  
            // ограничиваем до макс. числа светодиодов
            Rlenght = constrain(Rlenght, 0, segLen);
            Llenght = constrain(Llenght, 0, segLen);

            // инвертируем, если нужно
            if (invertSides) {
              maxLevel = Llenght; //  maxLevel используется как буфер
              Llenght = Rlenght;
              Rlenght = maxLevel;
            }

            // Для поиска технических неисправностей
            #if (DEV_LOG == 1)   
            //Serial.print(Llenght); Serial.print(' '); Serial.println(Rlenght);
            #endif
            
            // отрисовать  
            if (set.mode == 5) {
              for (int i = (segLen-1); i > (segLen - Llenght); i--)
                MusicEffectFill(i);
              for (int i = segLen; i < (segLen + Rlenght); i++ )
                MusicEffectFill(i);
            }
            else { // ~ set.mode == 6
              for (int i = 0; i < Llenght; i++)
                MusicEffectFill(i);
              for (int i = 2*segLen-1; i > (2*segLen - Rlenght); i-- )
                MusicEffectFill(i);
            }

            // смещаемся по "колесу"
            curpos++;
            if (curpos == distance) curpos=0;
            
            strip.show();
          }
    /*if (curpos % 8 == 0) interface(); // снежинки идут медленно, но эффект ленты приемлимый
    // interface(); //даёт задержку 39 мс (со снежинками) и 24 мс без анимации */
    // ограничение на отрисовку интерфейса для увеличения плавности эффекта
    // для удовлетворительной плавности нужно ~5 мс
        
    // если был устойчивый сигнал и прошло 30 шагов по "колесу", или нажали кнопку, или прошло 3 секунды:
    buttonCheck(ch);
    if ( ((RsoundLevel_f > 15 && LsoundLevel_f > 15) && (curpos % 30 == 0)) || newInput == 1 || millis()-timer > 3000)
      interface();
}

void MusicEffectFill (int i) {
  for (byte j=0; j<set.numwaves; j++)  
    strip.setPixelColor( (i+j*NUMPIXELS/set.numwaves), WheelOneColor( (((NUMPIXELS-i-1) * rainbow_part * set.numwaves / NUMPIXELS) + curpos) & 255, set.color) );
}

void autoLowPass() {
  /*// для режима VU
  int thisMax = 0;                          // максимум
  int thisLevel = analogRead(SOUND_R);
  delay(10);                                // ждём инициализации АЦП
  thisLevel = analogRead(SOUND_R); 
  for (byte i = 0; i < 200; i++) {
    thisLevel = analogRead(SOUND_R);        // делаем 200 измерений
    if (thisLevel > thisMax)                // ищем максимумы
      thisMax = thisLevel;                  // запоминаем
    delay(4);                               // ждём 4мс
  }
  LOW_PASS = thisMax + LOW_PASS_ADD;        // нижний порог как максимум тишины + некая величина
  #if (DEV_LOG == 1) 
  Serial.print(F("low_pass is ")); Serial.println(LOW_PASS);
  #endif*/
  
  /*// для режима спектра
  thisMax = 0;
  for (byte i = 0; i < 100; i++) {          // делаем 100 измерений
    analyzeAudio();                         // разбить в спектр
    for (byte j = 2; j < 32; j++) {         // первые 2 канала - хлам
      thisLevel = fht_log_out[j];
      if (thisLevel > thisMax)              // ищем максимумы
        thisMax = thisLevel;                // запоминаем
    }
    delay(4);                               // ждём 4мс
  }
  SPEKTR_LOW_PASS = thisMax + LOW_PASS_FREQ_ADD;  // нижний порог как максимум тишины
  if (EEPROM_LOW_PASS && !AUTO_LOW_PASS) {
    EEPROM.updateInt(70, LOW_PASS);
    EEPROM.updateInt(72, SPEKTR_LOW_PASS);
  }*/
}
