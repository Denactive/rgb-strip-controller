// interface / markselected / initializeAnimation / snowHorizontal
void interface(void) { // 39 мс
  
    if (set.mode != 5 && set.mode != 6) buttonCheck(ch);
    
    if (newInput == 1) {
      #if (DEV_LOG == 1) 
        Serial.print(F("registered button: ")); 
        Serial.println(ch);
      #endif
      nullButtonFlags();  // обнуляем флаги кнопок
          
      if(ch=='1' && pos>1) {pos--;}
      if(ch=='2' && pos<3) {pos++;}
      if(pos==1 && ch=='3'){
        menuPage(menuOptions, menuOptionsAmount, toDoList[0]);
        pos=1;
      }
      if (pos==0) pos=1;
      timer=millis();
     }   
     if (millis()-timer > 3000) {
       pos=0; 
       timer=millis();
     }
     display.clearDisplay();

    display.setTextSize(1);             // Upscalled 2:1 pixel scale
    display.setTextColor(SSD1306_WHITE);        // Draw white text
     
    switch(pos) {
      case 1: markSelected(field1,0,4,1); break;
      case 2: markSelected(field2,0,7,1); break;
      case 3: markSelected(field3,0,4,1); break;
     }
  
    if (set.mode != 5 && set.mode != 6) 
      switch(set.anim) {
        case 0: break;
        case 1: snowHorizontal(snowflakesSet, FLAKESIMAGESAMOUNT, LOGO_WIDTH, LOGO_HEIGHT, 0, 8, SCREEN_WIDTH, 23); break; 
      }

   display.setCursor(field1,0);         // Start at top-left corner
   display.println(F("MENU"));
   display.setCursor(field2,0);        
   display.println(F("FIELD 2"));
   display.setCursor(field3,0);        
   display.println(F("info"));

   // рисуем стрелочки < > 
   display.setCursor(0,10); display.print(char(17));
   display.setCursor(SCREEN_WIDTH-6,10); display.print(char(16));

   display.drawFastHLine(0, 8, SCREEN_WIDTH, 1);
   display.drawFastHLine(0, SCREEN_HEIGHT-1, SCREEN_WIDTH, 1);
   display.display();
}


void markSelected(uint8_t x, uint8_t y, uint8_t symAmount, uint8_t size)
{
  display.drawFastVLine(x-3,y, 8*size-1, 1);
  display.drawPixel(x-2,y,1);
  display.drawPixel(x-2,y+8*size-2,1);
  
  display.drawFastVLine(x+1+6*size*symAmount,y, 8*size-1, 1);
  display.drawPixel(x+6*size*symAmount,y,1);
  display.drawPixel(x+6*size*symAmount,y+8*size-2,1);
}


void initializeAnimation(void) {
    switch(set.anim) {
      case 0: break;
      case 1: {
        // Initialize 'snowflake' positions
        for(byte f=0; f< NUMFLAKES; f++) {
          icons[f][XPOS]   = - random(LOGO_WIDTH, 3*LOGO_WIDTH);
          icons[f][YPOS]   = random(16, SCREEN_HEIGHT-16);
          icons[f][DELTAY] = random(1, 4);
          icons[f][TYPE] = random (0, FLAKESIMAGESAMOUNT);
          #if (DEV_LOG == 1) 
            /*Serial.print(F("x: "));
            Serial.print(icons[f][XPOS], DEC);
            Serial.print(F(" y: "));
            Serial.print(icons[f][YPOS], DEC);
            Serial.print(F(" dy: "));
            Serial.print(icons[f][DELTAY], DEC);
            Serial.print(F(" type: "));
            Serial.println(icons[f][TYPE], DEC);*/
          #endif
        }
    } break;
    case 2: break;
  }
}


void snowHorizontal(const unsigned char** images, uint8_t imagesAmount, uint8_t bmpw, uint8_t bmph, uint8_t x, uint8_t y, uint8_t w, uint8_t h) {
    byte f;
    //Update coordinates of each flake... 
    for(f=0; f< NUMFLAKES; f++) {
      icons[f][XPOS] += icons[f][DELTAY];
      // If snowflake is off the edge of the screen...
      if (icons[f][XPOS] >= w) {
        // Reinitialize to a random position, just from the left
        icons[f][XPOS]   = - random(LOGO_WIDTH, 3*LOGO_WIDTH);
        icons[f][YPOS]   = random(y, y+h-LOGO_HEIGHT+1);
        icons[f][DELTAY] = random(1, 6);
        icons[f][TYPE] = random(0, FLAKESIMAGESAMOUNT-1);
      }
    }
    // draw new flakes
    for(f=0; f< NUMFLAKES; f++) {
      display.drawBitmap(icons[f][XPOS], icons[f][YPOS], images[icons[f][TYPE]], bmpw, bmph, SSD1306_WHITE);
    }
    //delay(delayval);        // Pause for 1/10 second
}
