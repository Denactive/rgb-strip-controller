#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

#define DEV_LOG 1
#define RESET_SETTINGS 0    // сброс настроек в EEPROM памяти (поставить 1, прошиться, поставить обратно 0, прошиться. Всё)
                            // [0] - задать начальные настройки отсюда (будут записаны в память, если менять их в меню) 
#define DEV_SETTINGS 0      // [1] - начальные настройки берутся из памяти
#define DEV_ANIM 1
#define DEV_COLOR 2
#define DEV_DIR 1
#define DEV_MODE 5
#define DEV_NUMWAVES 2
#define DEV_BRIGHT 255 // k*32 - 1
//#define DEV_WAIT 0 // пока нереализовано

struct settings {
  uint8_t anim;
  uint8_t color;
  uint8_t dir;
  uint8_t mode; 
  uint8_t numwaves;
  uint8_t bright;
  //uint8_t wait;
} set;

settings EEMEM constset;
uint8_t EEMEM initializeKeyAddress;
#define initializeKey 228
bool turn = 1;

//------------------------НАСТРОЙКИ ЛЕНТЫ-----------------------//
#define PIN_DIM 6           // управляющий ПИН для ленты
#define brightness 255
#define NUMPIXELS 60

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN_DIM, NEO_GRB+NEO_KHZ800);

//------------------------НАСТРОЙКИ МУЗЫКИ-----------------------//

// ----- Подключение
#define SOUND_R A2                // аналоговый пин вход аудио, правый канал
#define SOUND_L A1                // аналоговый пин вход аудио, левый канал

//#define SOUND_R_FREQ A3         // аналоговый пин вход аудио для режима с частотами (через кондер)
// ----- Визуальные настройки
#define invertSides 0             // поменять местами левую и правую части ленты местами, если требуется
#define rainbow_part  255         // 255 отображение половины радуги на ленте, 127 - целой, 85 - 2/3 (4.3 цвета), 45 - 1/3 (2 цвета)
#define backgroundR 20            // тёмно-фиолетовый отлично сочитается с яркими цветами радуги
#define backgroundG 0
#define backgroundB 40
// ----- сигнал
#define MONO 0                    // 1 - только один канал (ПРАВЫЙ!!!!! SOUND_R!!!!!), 0 - два канала
#define EXP 1.4                   // степень усиления сигнала (для более "резкой" работы) (по умолчанию 1.4)
#define POTENT 0                  // 1 - используем потенциометр, 0 - используется внутренний источник опорного напряжения 1.1 В
// ----- нижний порог шумов
#define minLevel 15               // минимальный уровень RsoundLevel_f и Lsound... для отрисовки на ленте
uint16_t LOW_PASS = 0;            // нижний порог шумов режим VU, ручная настройка
//uint16_t SPEKTR_LOW_PASS = 40;  // нижний порог шумов режим спектра, ручная настройка
//#define AUTO_LOW_PASS 0         // разрешить настройку нижнего порога шумов при запуске (по умолч. 0)
//#define EEPROM_LOW_PASS 1       // порог шумов хранится в энергонезависимой памяти (по умолч. 1)
#define LOW_PASS_ADD 0            // "добавочная" величина к нижнему порогу, для надёжности (режим VU)
//#define LOW_PASS_FREQ_ADD 3     // "добавочная" величина к нижнему порогу, для надёжности (режим частот)
// ----- режим шкала громкости
float SMOOTH = 0.3;               // коэффициент плавности анимации VU (по умолчанию 0.5)
#define MAX_COEF 1.8              // коэффициент громкости (максимальное равно срeднему * этот коэф) (по умолчанию 1.8)

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

uint8_t Rlenght, Llenght, curpos=0, segLen=NUMPIXELS/2;
float RsoundLevel, RsoundLevel_f, LsoundLevel, LsoundLevel_f, averageLevel = 50, averK = 0.006;
uint16_t RcurrentLevel, LcurrentLevel, maxLevel=100;

//------------------------НАСТРОЙКИ КНОПОК-----------------------//
#define buttonPin1 2
#define buttonPin2 3
#define buttonPinR 5 // 4 пин постоянно HIGH (задействован для OLED_RESET)
#define doubleClickDebounce 150
#define debounce 80
#define hold 300  // время задержки между кликами
bool button1S, button1F, button1P, button1R, button1D, button1DP;
bool button2S, button2F, button2P, button2R, button2D, button2DP;
bool buttonRS, buttonRF, buttonRP, buttonRR, buttonRD, buttonRDP;
bool newInput=0;
uint32_t clickTime, clickDoubleTime;

//------------------------ Цветная полоса --------------------
// плавно движущаяся  полоса, 1 цвет или эффект
// Настройки положения полосы и направления распространения полосы, количество полос, эффекты, цвет, отражение от краев или центра

// [DISTANCE] - отношение проходимого пути конкретного пикселя (цвета) к длине ленты | 97~1/3 | 127~1/2 | 255~1

// [NUMWAVES] количество полос | MIN=1 MAX=S | Если wavesAmount = кол-ву светодиодов (s) - пульсация для цветов и смена цветов для радуги и эффектов 

// [BRIGHT] - Яркость: MAX-255, MIN-1

// [MODE]       [0] Статический цвет
//              [1] Дыхание / плавная смена цветов для многоцветных эффектов
//              [2] Полоса бегает по кругу
//              [3] Полоса отражается от концов ленты
//              [4] 2 полосы отражаются от концов и друг от друга
//              [5] Светомузыка

// [DIRACTION]  [0] начало << ф,с,г,з,ж,о,к << конец
//              [1] начало << к,о,ж,з,г,с,ф << конец
//              [2] начало >> ф,с,г,з,ж,о,к >> конец
//              [3] начало >> к,о,ж,з,г,с,ф >> конец

// [COLOR]      [0] радуга
//              [1] красный
//              [2] желтый
//              [3] зелёный
//              [4] голубой
//              [5] синий
//              [6] пурпурный
//              [7] белый
//              [8] светофор
//              [9] сирена (с.ф.к)
//              [10] северное сияние
bool l1=1, l2=0;
byte distance=255, reflection2parametre=1, prevColor=0;
//=====================================================================//

//------------------------НАСТРОЙКИ СТРАНИЦ МЕНЮ-----------------------//
typedef void (*funcPointer_t)(uint8_t, char*); // тип "указателей на функцию ", чтобы передать в меню
void selectionMenu (uint8_t s, char*); // прототипы, чтобы записать в массив
void selectionAnim (uint8_t s, char*);
void selectionColor (uint8_t s, char*); 
void selectionDir (uint8_t s, char*);  
void selectionMode (uint8_t s, char*); 

void (*toDoList[])(uint8_t, char*)={selectionMenu, selectionAnim, selectionColor, selectionDir, selectionMode};

// служебные записи
const char optionExit[] PROGMEM = "exit";
//const char notCycleModeWarning[] PROGMEM = "cycle mode only";
const char lockedOptionWarning[] PROGMEM = "can't be set in this mode"; //"change mode to unlock this option"
//////////////////////////////////////////////////////////////////////////////
const char headerMenu[] PROGMEM = "<MENU>";
const char option1[] PROGMEM = "animation";
const char option2[] PROGMEM = "turn";
const char option3[] PROGMEM = "color";
const char option4[] PROGMEM = "diraction";
const char option5[] PROGMEM = "mode";
const char option6[] PROGMEM = "waves"; // for mode 0 only
const char option7[] PROGMEM = "brightness";

const char* const menuOptions[] PROGMEM = {
  headerMenu, option1, option2, option3,  option4, option5, option6, option7, optionExit
};
#define menuOptionsAmount 8 //не учитывать header
//////////////////////////////////////////////////////////////////////////////
const char headerMode[] PROGMEM = "<MODE>"; // lim 16 sym
const char optionMode1[] PROGMEM = "static";
const char optionMode2[] PROGMEM = "breathing";
const char optionMode3[] PROGMEM = "cycle";
const char optionMode4[] PROGMEM = "reflection";
const char optionMode5[] PROGMEM = "resistance";
const char optionMode6[] PROGMEM = "music waves";
const char optionMode7[] PROGMEM = "inverted music";

const char* const modeOptions[] PROGMEM = {
  headerMode, optionMode1, optionMode2, optionMode3, optionMode4, optionMode5, optionMode6, optionMode7, optionExit,
};
#define modeOptionsAmount 8 //не учитывать header / учитывать exit
//////////////////////////////////////////////////////////////////////////////
const char headerColor[] PROGMEM = "<COLOR>"; //lim = SCREENWIDTH/6 - 1 - максимально возможное кол-во символов в строке для корректного отображения в подменю
const char optionColor1[] PROGMEM = "Rainbow";
const char optionColor2[] PROGMEM = "red";
const char optionColor3[] PROGMEM = "yellow";
const char optionColor4[] PROGMEM = "green";
const char optionColor5[] PROGMEM = "light blue";
const char optionColor6[] PROGMEM = "blue";
const char optionColor7[] PROGMEM = "magenta";
const char optionColorW[] PROGMEM = "white";
const char optionColor8[] PROGMEM = "traffic light";
const char optionColor9[] PROGMEM = "infrared galaxy";
const char optionColor10[] PROGMEM = "aurora";
//const char optionColor11[] PROGMEM = "";
//const char optionColor12[] PROGMEM = "";

const char* const colorOptions[] PROGMEM = {
  headerColor, optionColor1, optionColor2, optionColor3, optionColor4, optionColor5, optionColor6, optionColor7, optionColorW, optionColor8, optionColor9, 
                   optionColor10, optionExit,
};
#define colorOptionsAmount 12 //не учитывать header / учитывать exit
//////////////////////////////////////////////////////////////////////////////
const char headerDiraction[] PROGMEM = "<DIRACTION>"; // lim 16 sym
const char optionDiraction1[] PROGMEM = "<straight order<";
const char optionDiraction2[] PROGMEM = "<inverted order<";
const char optionDiraction3[] PROGMEM = ">straight order>";
const char optionDiraction4[] PROGMEM = ">inverted order>";
/*const char optionDiraction1[] PROGMEM = "<-<";
const char optionDiraction2[] PROGMEM = "<+<";
const char optionDiraction3[] PROGMEM = ">->";
const char optionDiraction4[] PROGMEM = ">+>";*/

const char* const diractionOptions[] PROGMEM = {
  headerDiraction, optionDiraction1, optionDiraction2, optionDiraction3, optionDiraction4, optionExit,
};
#define diractionOptionsAmount 5 //не учитывать header / учитывать exit
//////////////////////////////////////////////////////////////////////////////
const char headerAnim[] PROGMEM = "<ANIMATION>"; // lim 16 sym
const char optionAnim1[] PROGMEM = "off";
const char optionAnim2[] PROGMEM = "snow wind";

const char* const animOptions[] PROGMEM = {
  headerAnim, optionAnim1, optionAnim2, optionExit,
};
#define animOptionsAmount 3 //не учитывать header / учитывать exit
//=====================================================================//

 
//------------------------НАСТРОЙКИ ЭКРАНА----------------------------//

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32   // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
//=====================================================================//




//------------------------НАСТРОЙКИ АНИМАЦИИ---------------------------//
#define NUMFLAKES     10 // Number of particles in the animation
#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16
/*
static const unsigned char PROGMEM empty[] =
{ B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000,
  B00000000, B00000000 };
  */
  
static const unsigned char PROGMEM snowflake1[] =
{ B00000010, B10000000,
  B01001001, B01010010,
  B00101101, B00110100,
  B00001110, B01110000,
  B01110110, B01101110,
  B00111010, B01011100,
  B01011101, B10111001,
  B10000010, B01000110,
  B01100010, B01000001,
  B10011100, B10111010,
  B00111010, B01011100,
  B01110110, B01101110,
  B00001110, B01110000,
  B00101100, B10110100,
  B01001010, B10010010,
  B00000001, B01000000 };

static const unsigned char PROGMEM snowflake2[] =
{ B00000001, B00000000,
  B00000001, B10000000,
  B00110100, B10101100,
  B00110000, B00001100,
  B00001101, B00110000,
  B00101101, B10110100,
  B00000011, B11000000,
  B01100110, B01110011,
  B11001110, B01100110,
  B00000011, B11000000,
  B00101101, B10110100,
  B00001100, B10110000,
  B00110000, B00011000,
  B00110101, B01011000,
  B00000001, B10000000,
  B00000000, B10000000 };


static const unsigned char PROGMEM snowflake3[] =
{ B00001001, B10000000,
  B01100100, B10100110,
  B01000100, B10100010,
  B00010011, B11001000,
  B00001001, B10010001,
  B01100110, B01100110,
  B00010101, B10101000,
  B11111011, B11011001,
  B10011011, B11011111,
  B00010101, B10101000,
  B01100110, B01100110,
  B10001001, B10010000,
  B00010011, B11001000,
  B01000101, B00100010,
  B01100101, B00100110,
  B00000001, B10010000 };
  
static const unsigned char PROGMEM adafruit_star_flakes[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000 };

static const unsigned char PROGMEM ussr_logo[] =
{ B00000000, B00000000,
  B00000000, B01000000,
  B00000000, B00110000,
  B00000111, B10011000,
  B00001111, B00001100,
  B00011110, B00000110,
  B00011111, B00000110,
  B00001001, B10000110,
  B00000000, B11000110,
  B00000000, B01100110,
  B00000100, B00111110,
  B00011110, B00111100,
  B00111011, B11111100,
  B01100000, B11100110,
  B00000000, B00000011,
  B00000000, B00000000 };
  
static const unsigned char PROGMEM svastic[] =
{ B00010000, B11111110,
  B00110000, B11111111,
  B00110000, B11000000,
  B00110000, B11000000,
  B00110000, B11000000,
  B00110000, B11000000,
  B00111111, B11111111,
  B00111111, B11111111,
  B00000000, B11000011,
  B00000000, B11000011,
  B00000000, B11000011,
  B00000000, B11000011,
  B00111111, B11000011,
  B00011111, B11000010,
  B00000000, B00000000,
  B00000000, B00000000, };


static const unsigned char* snowflakesSet[] = {
  snowflake1, snowflake2, snowflake3, adafruit_star_flakes
  };
#define FLAKESIMAGESAMOUNT  4  // Number of different snowflakes BMPs
#define XPOS   0 // Indexes into the 'icons' array in function below
#define YPOS   1
#define DELTAY 2
#define TYPE 3
int8_t  icons[NUMFLAKES][4];
//=====================================================================//

//------------------------interface-----------------------//
#define field1 3
#define field2 34
#define field3 99
#define fieldScrolling 0 // нереализовано
char ch='\0';
uint8_t pos=0;
uint32_t timer;
//=====================================================================//


void setup() {
  //инициализация порта
  #if (DEV_LOG == 1) 
  Serial.println(F("Введите 1, 2, 3 >> "));
  Serial.begin(9600);
  #endif

  //инициализация кнопок
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(buttonPinR, INPUT);

  //инициализация шняг для светомузыки
  if (POTENT) analogReference(EXTERNAL);
  else
  #if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
      analogReference(INTERNAL1V1);
  #else
      analogReference(INTERNAL);
  #endif
  
  // жуткая магия, меняем частоту оцифровки до 18 кГц
  // команды на ебучем ассемблере, даже не спрашивайте, как это работает
  // поднимаем частоту опроса аналогового порта до 38.4 кГц, по теореме
  // Котельникова (Найквиста) частота дискретизации будет 19.2 кГц
  // http://yaab-arduino.blogspot.ru/2015/02/fast-sampling-from-analog-input.html
  sbi(ADCSRA, ADPS2);
  cbi(ADCSRA, ADPS1);
  sbi(ADCSRA, ADPS0);

  //инициализация дисплея
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    #if (DEV_LOG == 1) 
    Serial.println(F("SSD1306 allocation failed"));
    #endif
     for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay(); 
  display.setTextSize(2);           
  display.setTextColor(SSD1306_WHITE);       
  display.setCursor(12,8);    
  display.setTextWrap(0);     
  display.println(F("DENACTIVE"));
  display.setTextSize(2); 
  display.setCursor(12,24);
  display.print(F("Denis Turchin Co"));
  
  display.display();
  delay(2000);
  display.clearDisplay(); 
  
  /*timer=millis();  Serial.print("interface function runtime is ");
  interface();
  Serial.println(millis()-timer);*/
  
  /*if (AUTO_LOW_PASS && !EEPROM_LOW_PASS) {         // если разрешена автонастройка нижнего порога шумов
    autoLowPass();
  }
  if (EEPROM_LOW_PASS) {                // восстановить значения шумов из памяти
    //LOW_PASS = EEPROM.readInt(70);
    //SPEKTR_LOW_PASS = EEPROM.readInt(72);
  }*/
  //инициализация начальных значений настроек
  if (RESET_SETTINGS) EEPROM.write((int)&initializeKeyAddress, 0);       // сброс флага настроек
  
  //Проверка: первый ли запуск системы
  if (EEPROM.read((int)&initializeKeyAddress) != initializeKey) {
    EEPROM.write((int)&initializeKeyAddress, initializeKey);
    set.anim=1; set.color=0; set.mode=3;
    set.dir=1; set.numwaves=1; set.bright=64; //set.wait=0;
    EEPROM.put((int)&constset, set);
  }
  /*if (set.anim < 1) set.anim=1;         if (set.color < 0) set.color=0;
  if (set.mode < 0) set.mode=0;         if (set.dir < 1) set.dir=1;
  if (set.numwaves < 1) set.numwaves=1; if (set.bright < 16) set.bright=16;*/
  if (DEV_SETTINGS) {
    set.anim=DEV_ANIM; set.color=DEV_COLOR; set.mode=DEV_MODE;
    set.dir=DEV_DIR; set.bright=DEV_BRIGHT; //set.wait=DEV_WAIT;
  } 
  else
    EEPROM.get((int)&constset, set); 
  
  //инициализация ленты
  strip.begin(); 
  strip.setBrightness(brightness);
  strip.show();

  //инициализация эффектов
  selectionMode(set.mode, NULL);
  if (DEV_SETTINGS) 
    set.numwaves=DEV_NUMWAVES;
  segLen=NUMPIXELS/2/set.numwaves; // проинициализировать длину сегмента в соотвенствии
                                   // с сохраненным numwaves для режима светомузыки
  initializeAnimation();
  interface();
}


void loop() {
 if (set.mode != 5 && set.mode != 6) rainbowCycle(set.color);
 else musicSoundShow(); 
}
