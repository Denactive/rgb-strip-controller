// rainbowCycle / WheelOneColor
//------------------------ ПЛАВНАЯ ПОЛОСА --------------------
// Цвета добaвлять в WheelOneColor. Обрати внимание на первую строку этой функции

void rainbowCycle(uint8_t color) {
  uint16_t j;
  if (l1) {    
    for(j=0; j<distance; j++) {
      rainbowCycleBody(j);
      if (set.mode > 2) {l1=0;} // 3 или 4 - (5/6-светомузыки. эта функция не задействуется)
      //if (set.mode == 3) {color=11;}
    }
  }
  else {
    for(j=distance; j>0; j--) {
      rainbowCycleBody(j);
      if (set.mode > 2) {l1=1;}
    }
  }
}

void rainbowCycleBody(uint16_t j) {
  uint16_t i;
  uint32_t curColor=0;
  if(j%4==0) interface();
    for (i=0; i < NUMPIXELS/reflection2parametre; i++) {
      if (!l2) {        
        curColor = WheelOneColor(((i * 256 / NUMPIXELS * set.numwaves) + j) & 255, set.color);
                  
        strip.setPixelColor(i, curColor);
        if(reflection2parametre == 2)
          strip.setPixelColor(NUMPIXELS-i-1, curColor);
      }
      else {
        curColor = WheelOneColor( (256-(i * 256 / NUMPIXELS * set.numwaves) + j) & 255, set.color);
        
        strip.setPixelColor(i, curColor);
        if(reflection2parametre == 2) 
          strip.setPixelColor(NUMPIXELS-i-1, curColor);
      }  
      /*if(reflection2parametre == 2) 
          strip.setPixelColor(NUMPIXELS-i-1, strip.getPixelColor(i)); // жрёт 572 байта ПЗУ */
    }
  strip.show();
}

uint32_t WheelOneColor(byte WheelPos, byte color) {
    //if((color<0)||(color>12)){color=0;}
    byte n = 255 / set.bright; //n++;
    bool staticColor = 0;
    if (set.mode == 0 || set.mode == 5 || set.mode == 6) staticColor = 1;
    
    switch(color) {
        
      case 0: // радуга
      if(WheelPos < 85) {
        return strip.Color(WheelPos * 3 /n, (255 - WheelPos * 3)/n , 0);
      } else if(WheelPos < 170) {
        WheelPos -= 85; return strip.Color((255 - WheelPos * 3)/n , 0, WheelPos * 3 / n );
      } else {
        WheelPos -= 170;return strip.Color(0, WheelPos * 3 / n, (255 - WheelPos * 3)/n );
      } break;

     //                                 моноцвета                             //
      case 1: //red
        if(staticColor) return strip.Color(255/n, 0, 0); //static
        if(WheelPos < 85) {
          return strip.Color(0,0,0);
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(WheelPos * 3 /n, 0 , 0);
        } else {
         WheelPos -= 170;
          return strip.Color((255 - WheelPos * 3)/n , 0, 0 );
        } break;
        
      case 2: //желтый
        if(staticColor) return strip.Color(255/n, 255/n, 0); //static
        if(WheelPos < 85) {
        return strip.Color(0,0,0);
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(WheelPos * 3 /n, WheelPos * 3 /n , 0);
        } else {
         WheelPos -= 170; return strip.Color((255 - WheelPos * 3)/n , (255 - WheelPos * 3)/n, 0 );
        } break;  
        
      case 3: //green
        if(staticColor) return strip.Color(0, 255/n, 0); //static
        if(WheelPos < 85) {
        return strip.Color(0, 0, 0 );
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(0, WheelPos * 3 / n, 0);
        } else {
         WheelPos -= 170; return strip.Color(0, (255 - WheelPos * 3)/n , 0);
        } break;
        
      case 4: //голубой
        if(staticColor) return strip.Color(0, 255/n, 255/n); //static
        if(WheelPos < 85) {
        return strip.Color(0,0,0);
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(0, WheelPos * 3 /n , WheelPos * 3 /n);
        } else {
         WheelPos -= 170; return strip.Color(0 , (255 - WheelPos * 3)/n, (255 - WheelPos * 3)/n );
        } break;

      case 5: //blue
        if(staticColor) return strip.Color(0, 0, 255/n); //static
        if(WheelPos < 85) {
        return strip.Color(0, 0 , 0);
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(0 , 0, WheelPos * 3 / n );
        } else {
         WheelPos -= 170;return strip.Color(0, 0, (255 - WheelPos * 3)/n );
        } break;
        
      case 6: //пурпур
        if(staticColor) return strip.Color(255/n, 0, 255/n); //static
        if(WheelPos < 85) {
        return strip.Color(0,0,0);
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(WheelPos * 3 /n , 0, WheelPos * 3 /n );
        } else {
         WheelPos -= 170;return strip.Color((255 - WheelPos * 3)/n, 0, (255 - WheelPos * 3)/n);
        } break;

      case 7: //white
        if(staticColor) return strip.Color(255/n, 255/n, 255/n); //static
        if(WheelPos < 85) {
        return strip.Color(0, 0, 0 );
        } else if(WheelPos < 170) {
         WheelPos -= 85; return strip.Color(WheelPos * 3 / n, WheelPos * 3 / n, WheelPos * 3 / n);
        } else {
         WheelPos -= 170; return strip.Color((255 - WheelPos * 3)/n, (255 - WheelPos * 3)/n , (255 - WheelPos * 3)/n);
        } break;
      
      case 8: //светофор к ж з
        if(WheelPos < 128) {
        return strip.Color(WheelPos * 2 /n, (255 - WheelPos * 2)/n , 0);
        } else  {
         WheelPos -= 128; return strip.Color((255 - WheelPos * 2)/n , WheelPos * 2 / n, 0 );
        } break;

      case 9: //светофор с ф к Сирена
        if(WheelPos < 128) {
        return strip.Color(WheelPos * 2 /n, 0, (255 - WheelPos * 2)/n);
        } else  {
         WheelPos -= 128; return strip.Color((255 - WheelPos * 2)/n ,0, WheelPos * 2 / n );
        } break;
        
      case 10: // Северное сияние
        if(WheelPos < 128) {
        return strip.Color(0, WheelPos * 1 /n, (255 - WheelPos * 2)/n);
        } else  {
         WheelPos -= 128; return strip.Color(0, (127 - WheelPos * 1)/n , WheelPos * 2 / n );
        } break;

      /*case 11: // сине-белый
        if(WheelPos < 64) {
        return strip.Color(0, 0, 0);
        } else if(WheelPos < 128) {
         WheelPos -= 64; return strip.Color(0 , 0, WheelPos * 4 / n );
        } else if(WheelPos <192) {
         WheelPos -= 128;return strip.Color(WheelPos * 4 / n, WheelPos * 4 / n, 255 );
        } else { 
          WheelPos -= 192; return strip.Color((255 - WheelPos * 4)/n,(255 - WheelPos * 4)/n,(255 - WheelPos * 4)/n);
        } break;
        
      case 12: // сине-белый ассиметричный 10
        if(WheelPos < 64) {
        return strip.Color(0, 0, 0);
        } else if(WheelPos < 128) {
         WheelPos -= 64; return strip.Color(WheelPos * 4 / n  , WheelPos * 4 / n , WheelPos * 4 / n );
        } else if(WheelPos <192) {
         WheelPos -= 128;return strip.Color((255 - WheelPos * 4)/n, (255 - WheelPos * 4)/n, 255 );
        } else { 
          WheelPos -= 192; return strip.Color(0, 0,(255 - WheelPos * 4)/n);
        } break;*/
        
      default: return 0; break;
  }
  
}
