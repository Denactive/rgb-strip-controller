[Версия на русском](./README_RUS.md)

# <a name="top"></a>The RGB-SC project. 
## Description ##

A 3pin RGB-strip controller based on the Arduino Nano with a display and different light effects.

An animation of 8x8 pixels one color images floating and a menu are being displayed while work.
The device has different light waves movement effects and build-in color palettes, which are free to customize.
There is an music-compible effect which makes light waves move together with a music beat.

Version **v9.0**

## Developer ##
- [Denis Kirillov](https://gitlab.com/Denactive)

## Presentation ##
[Media](https://drive.google.com/drive/folders/1-qnXHuQfeRG2hpFx-zkutehT0nIrXHhb?usp=sharing)

## Software requirements ##

- Arduino IDE. ATmega328P Old-loader
- Adafruit_GFX_Library
- Adafruit_NeoPixel for 3pin RGB-strips
- Adafruit_SSD1306 or simillar for other displays

## <a name="components"></a>Components ##

- 1x The Arduino Nano board
- 1x 4pin SSD1306 display
- 2x 3.5mm audio-jack nests
- 3x (or [2*](#fst-tip)) buttons
- 3x 680 Ohm resistors
- 3x 6,8 kOhm resistors
- 1x 3pin RGB-strip of [any**](#snd-tip) lenght
- 1x 5V 1A DC power supply and a nest for its connector

<a name="fst-tip"></a>(*) The third button is concidered to be placed on the back of the device and is used as a confirm button (check buttonPinR option).
This is needed as firstly it was supposed to use a doubleclick on any of two buttons as a confirm action but cheap buttons
actually tend to stuck which makes user unable to treat with the device intuitively. Double clicks are registered by the program.
Moreover, latency can get really high as the controller gets loaded. 
<br/>
<a name="snd-tip"></a>(**) Currunt in the strip can be counted as 2А per 100 light diodes.
Present resistance values are suituble to use for 58 light diodes and 1A 5V DC source. The maximum brightness thus is really high for domestic use.

If you are planning to increase the power, I suggest to add another power supply for the strip. It is easy to do as light-strips has appropriate construction.

If you are planning to increase the power, I suggest to separate a high power strip curcuitry an a low power controller surroundings.

I do not recommend to use the device with high amount (> 60) of light diodes powered with Arduino Nano microUSB because:
1. A brightness in decreasing dramatically
2. The controller does not work long with current in it equal to 0.5A or more

## Build and installation ##
1. Build the device curcuitry with [these components](#components) using the [scheme](./scheme.pdf)
2. Save this repository on your PC and open as an ArduinoIDE project
3. Configure the [main.ino](./main.ino). This is the program entry point
4. Connect Arduino Nano via USB
5. Set the Old-loader and iterate by COM-ports to find out one connected to the Arduino
6. Load and enjoy

## Thanks to ##
- [Adafruit communtity](https://www.hackster.io/adafruit) < They have useful guides
- [AlexGyver's](https://alexgyver.ru/colormusic) homemade projects guides < Thanks for inspiration

## LICENSE ##
[LICENSE](./LICENSE.md)

You are allowed to modify any code in this repository and use it in your projects. Please, links a [my lisense](./LICENSE.md) file and
put this repository URL into your Readme.md.
Notice that a display consumes many Arduino resources RAM espessialy, thus it is hard to bring new features.
In the moment this code uses 1.5-1.9 kB from 2 available.  

