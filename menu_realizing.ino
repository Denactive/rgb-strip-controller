//menuPage / "switchfunctions"

void menuPage(const char* const menuopt[], const byte optionsAmount, funcPointer_t switchFunction)
{
  //Проверено: menuopt - тот же адрес, что и menuOptions  1000000000%
  
  char** optionsNamesBuffer = new char* [optionsAmount+1];
 
  for (byte i=0; i < optionsAmount+1; i++) {
    optionsNamesBuffer[i]= new char [strlen_P( (char*)pgm_read_word(&(menuopt[i])) ) + 1];
    strcpy_P(optionsNamesBuffer[i], (char*)pgm_read_word(&(menuopt[i])) ); 
  }

  byte p=1, k=0, select=0, exitMenuTime = 10;
  int8_t len=0, ppos=0, tpos=0; //pixel / text position  
  char input ='4', text[12];
  char** curSet;
  timer=millis();
  
  if (menuopt == menuOptions) {
    exitMenuTime = 20;  // в главном меню 20 секунд на выбор. В подменю - 10
    curSet = new char* [optionsAmount];    
    }
    
  while (input!='0' && (millis()-timer) < exitMenuTime*1000) {   
    display.clearDisplay();
    buttonCheck(input);
    
    if( newInput == 1) {
      // обнуляем флаги кнопок
      nullButtonFlags(); 
      timer = millis();
      #if (DEV_LOG == 1) 
        Serial.print(F("button: ")); 
        Serial.print(input);
      #endif
      
      if(k > 0 && p == 1 && input== '1') {k--; select--;}
      if(p > 1 && input == '1'){p--; select--;}
      if(k < optionsAmount-3 && p == 3 && input== '2') {k++; select++;}
      if(p < 3 && input == '2'){p++; select++;}
      ppos=0; tpos=0;
      #if (DEV_LOG == 1) 
        Serial.print(F(" position: ")); Serial.println(select);
      #endif
      
      if(input=='3'){
        switchFunction(select, &input);   // select [0..optionsAmount-1]
        if (menuopt != menuOptions && input!='0') {display.setCursor(SCREEN_WIDTH-56, 0); display.print(F("applied"));} // сообщение о принятом изменении
        timer=millis();
      }
    }

    if (menuopt == menuOptions) { // обращай внимание на ЗНАЧЕНИЯ, принимаемые настройками. заголовок подменю всегда имеет индекс [0]
      if (set.mode != 5 && set.mode != 6) {curSet[0] = new char [strlen_P( (char*)pgm_read_word(&(animOptions[ set.anim + 1 ])) ) + 1]; strcpy_P(curSet[0], (char*)pgm_read_word(&(animOptions[ set.anim + 1 ])));}
      else { curSet[0] = new char [strlen_P( lockedOptionWarning ) + 1]; strcpy_P(curSet[0], lockedOptionWarning); }
  
      curSet[1] = new char [9];
      if (turn == 0) { curSet[1][0] = 'o'; curSet[1][1] = 'n'; curSet[1][2] = ' '; curSet[1][3] = '/'; curSet[1][4] = ' '; curSet[1][5] = 'O'; curSet[1][6] = 'F'; curSet[1][7] = 'F'; curSet[1][8] = '\0'; } //"on / OFF"; 
      else           { curSet[1][0] = 'O'; curSet[1][1] = 'N'; curSet[1][2] = ' '; curSet[1][3] = '/'; curSet[1][4] = ' '; curSet[1][5] = 'o'; curSet[1][6] = 'f'; curSet[1][7] = 'f'; curSet[1][8] = '\0'; } //"ON / off"; 
  
      if (set.color != 255) {curSet[2] = new char [strlen_P( (char*)pgm_read_word(&(colorOptions[ set.color + 1 ])) ) + 1]; strcpy_P(curSet[2], (char*)pgm_read_word(&(colorOptions[ set.color + 1 ])) );} //color 0-...
      else {curSet[2] = new char [4]; curSet[2][0] = 'o'; curSet[2][1] = 'f'; curSet[2][2] = 'f'; curSet[2][3] = '\0';}
      
      if (set.mode == 2 || set.mode == 3 || set.mode == 4) { curSet[3] = new char [strlen_P( (char*)pgm_read_word(&(diractionOptions[ set.dir + 1])) ) + 1]; strcpy_P(curSet[3], (char*)pgm_read_word(&(diractionOptions[ set.dir + 1])) ); } // diraction 0-3
      else { curSet[3] = new char [strlen_P( lockedOptionWarning ) + 1]; strcpy_P(curSet[3], lockedOptionWarning ); }
      
      curSet[4] = new char [strlen_P( (char*)pgm_read_word(&(modeOptions[ set.mode + 1 ])) ) + 1]; strcpy_P(curSet[4], (char*)pgm_read_word(&(modeOptions[ set.mode + 1 ]))   ); // mode 0-5
      
      if (set.mode == 2 || set.mode == 3 || set.mode == 5 || set.mode == 6) { curSet[5] = new char [3]; curSet[5][0] = 'w';  curSet[5][1] = '\0'; }// amount of waves
      else { curSet[5] = new char [strlen_P( lockedOptionWarning ) + 1]; strcpy_P(curSet[5], lockedOptionWarning); }
      
      curSet[6] = new char [3]; curSet[6][0] = 'b'; curSet[6][1] = '\0'; // brightness        
  
      curSet[optionsAmount-1] = new char [1]; curSet[optionsAmount-1][0] = '\0'; //exit
      //for (byte j=0; j<optionsAmount; j++) Serial.println(curSet[j]);
    }

    display.setTextColor(SSD1306_WHITE);
    display.setTextSize(1);
    
    for (byte i=1; i<SCREEN_HEIGHT/8; i++) {  //SCREEN_HEIGHT/8 - кол-во строк, умещающихся на дисплее по высоте. 8 - высота 1 символа шрифтом 1. для 32 - 4 строки; для 64 - 8. Следи, чтобы опций меню хватало
      if (menuopt == menuOptions) {            // settings list
        if (strlen(curSet[k+i-1]) > 10) {
          for (byte j=0; j<10; j++) {
            display.setCursor(63+6*j, 8*i);
            display.print(curSet[k+i-1][j]);
          }
        }
        else {
          display.setCursor(63, 8*i);
          display.print(curSet[k+i-1]);
        }
        
        // заменяем кодовые слова w и b соответствующими значениями set.numWaves (amount of waves) и set.bright 
        
        const char aw[] = "w", br[] = "b";
        if (strcmp(aw, curSet[k+i-1]) == 0) {
          display.fillRect(63, 8*i, 12, 8, SSD1306_BLACK);
          if (set.numwaves / 10 == 0) display.setCursor(SCREEN_WIDTH - 12, 8*i); else display.setCursor(SCREEN_WIDTH - 18, 8*i);
          display.print(set.numwaves);
          display.setCursor(66, 8*i); display.print(char(16));
        }
        if (strcmp(br, curSet[k+i-1]) == 0) {
          display.fillRect(63, 8*i, 12, 8, SSD1306_BLACK);
          if (set.bright / 10 == 0) display.setCursor(SCREEN_WIDTH - 12, 8*i);
          else {if (set.bright / 100 == 0) display.setCursor(SCREEN_WIDTH - 18, 8*i); else display.setCursor(SCREEN_WIDTH - 24, 8*i);}
          display.print( set.bright + 1 ); 
          display.setCursor(66, 8*i); display.print(char(16));
        }
        

        //если длина выбранного параметра больше (SCREEN_WIDTH - 63)/6 символов (первые 10 символов - зарезервированы под наименование опции) 6 -ширина символа - сколлинг
        len = strlen(curSet[select]); //     pixel / text position  [0][0] - отображение с первой по десятую буквы ([0-9])
        if (len > 10 && i == p) {
          display.fillRect(63, 8*i, 6*11, 8, SSD1306_BLACK);

          if(ppos == 0) {
            if (tpos >=0 && tpos <= len - 11) { strncpy(text, curSet[select]+tpos, 11); text[11]='\0';} 
            if (tpos > len - 11 && tpos <= len) { strncpy(text, curSet[select]+tpos, len-tpos);  for (byte j=len-tpos; j<11; j++) text[j]=' ';  text[11]='\0';}
            if (tpos > len-1) tpos = -11;
            if (tpos < 0) { for (byte j=tpos; j<0; j++)  text[j-tpos] = ' '; strncpy(text-tpos, curSet[select], 11+tpos); text[11]='\0'; }
          }
          display.setCursor(63-ppos, 8*i);
          display.print(text);
          display.fillRect(58, 8*i, 5, 8, SSD1306_BLACK);
          display.fillRect(SCREEN_WIDTH-6, 8*i, 6, 8, SSD1306_BLACK);
          ppos++;
          if (ppos > 5) { ppos=0; tpos++; }
          
        }
        
      }

      display.setCursor(3, 8*i);                //menu options list
      display.print(optionsNamesBuffer[k+i]);
    }

    switch(p) {
    case 1: markSelected(3,8, strlen(optionsNamesBuffer[k+1]),1); break;
    case 2: markSelected(3,16,strlen(optionsNamesBuffer[k+2]),1); break;
    case 3: markSelected(3,24,strlen(optionsNamesBuffer[k+3]),1); break;
    }
    
    // индикатор позиции в списке
    display.fillRect(SCREEN_WIDTH-5, 8+k*uint16_t((SCREEN_HEIGHT-16)/(optionsAmount-2) ), 5, uint16_t( (SCREEN_HEIGHT-16)/(optionsAmount-2) ), SSD1306_WHITE); 
    display.setCursor(3, 0); display.print(optionsNamesBuffer[0]);                         // <header>
    display.setCursor(SCREEN_WIDTH-5,0); display.print(char(30));                          //  ^          
    display.setCursor(SCREEN_WIDTH-5,SCREEN_HEIGHT-7); display.print(char(31));            //  v 
    
    if (millis()-timer < 10000) { 
      if (menuopt == menuOptions) {display.setCursor(SCREEN_WIDTH-18,0); display.print(1);}
      display.setCursor(SCREEN_WIDTH-12,0); display.print(char( 57-(millis()-timer)/1000) ); 
      }
    else { display.setCursor(SCREEN_WIDTH-12,0); display.print(char( 67-(millis()-timer)/1000) ); } //  счетчик 10 (9) сек
    
    display.display();
    if (menuopt == menuOptions) {
      for (byte j=0; j<optionsAmount; j++) delete [] curSet[j];
    } 
  }
  // ^ while ^
  
  for (byte j=0; j<optionsAmount+1; j++) delete [] optionsNamesBuffer[j];
  delete [] optionsNamesBuffer;
  
  display.setTextSize(1);
  display.fillRect(SCREEN_WIDTH-60, 0, 54, 6, SSD1306_BLACK);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(SCREEN_WIDTH-30,0);
  display.print(F("exit")); 
  display.display(); delay(200);
  
  if (menuopt == menuOptions) { 
    
    delete [] curSet;
    
    segLen=NUMPIXELS/2/set.numwaves;
    
    if (turn == 0) {set.color = prevColor;} 
    EEPROM.put((int)&constset, set); 
    if (turn == 0) {set.color = 255;} 
    }
}


void selectionMenu (uint8_t s, char *input) {
  #if (DEV_LOG == 1) 
    Serial.println(F("switch:menu"));
  #endif
  switch(s){
    case 0: if (set.mode != 5 && set.mode != 6) menuPage(animOptions, animOptionsAmount, toDoList[1]);                            break; // anim 
    case 1: if (turn == 1) {turn = 0; prevColor=set.color; set.color=255;} else {turn=1; set.color=prevColor;}                    break; // turn
    case 2: menuPage(colorOptions, colorOptionsAmount, toDoList[2]);                                                              break; // color
    case 3: if (set.mode == 2 || set.mode == 3 || set.mode == 4) menuPage(diractionOptions, diractionOptionsAmount, toDoList[3]); break; // diraction
    case 4: menuPage(modeOptions, modeOptionsAmount, toDoList[4]);                                                                break; // mode
    case 5: if (set.mode == 2 || set.mode == 3 || set.mode == 5 || set.mode == 6)                     
              if (set.numwaves < 4) set.numwaves++; 
              else set.numwaves = 1;                                                                                              break; // waves   
    case 6: if (set.bright < 255) {set.bright+=32;} else {set.bright = 31;}                                                       break; // brightness
    case 7: (*input) = '0'; break;
  }
}


void selectionAnim (uint8_t s, char *in) {
  #if (DEV_LOG == 1) 
    Serial.println(F("switch:animation"));
    #endif
  
  if (0 <= s && s <= (animOptionsAmount-2)) set.anim = s;
  if (s == (animOptionsAmount-1)) (*in) = '0';
  
  initializeAnimation();
}


void selectionColor (uint8_t s, char *in) {
  #if (DEV_LOG == 1) 
    Serial.println(F("switch:color"));
  #endif
  /*switch(s){
    case 0: set.color=0; break;
    case 1: set.color=1; break;
    case 2: set.color=2; break;
    case 3: set.color=3; break;
    case 4: set.color=4; break;
    case 5: set.color=5; break;
    case 6: set.color=6; break;
    case 7: set.color=7; break;
    case 8: set.color=8; break;
    case 9: set.color=9; break;
    case 10: set.color=10; break;
    case 11: (*in) = '0'; break;
  }*/
  if (0 <= s && s <= (colorOptionsAmount-2)) set.color=s;
  if (s == (colorOptionsAmount-1)) (*in) = '0';
  if (!turn) prevColor = set.color;
}


void selectionDir(uint8_t s, char *in) {
  #if (DEV_LOG == 1) 
    Serial.println(F("switch:diraction"));
  #endif
  switch(s){
    case 0: set.dir=0; l1= 0; l2 = 1; break;
    case 1: set.dir=1; l1= 1; l2 = 0; break;
    case 2: set.dir=2; l1= 1; l2 = 1; break;
    case 3: set.dir=3; l1= 0; l2 = 0; break; 
    case 4: (*in) = '0'; break;
  }
}


void selectionMode (uint8_t s, char *in) {
  #if (DEV_LOG == 1) 
    Serial.println(F("switch:mode"));
  #endif
  
  if (s != set.mode && s != (modeOptionsAmount-1)) {
    // сброс настроек при смене режима с set.mode на s
    if (set.mode == 0 || set.mode == 1 || set.mode == 2)
      delay(10);
    else
      // чтобы после выхода из режимов 1 и 4 по умолчанию стояла 1 волна
      set.numwaves = 1; 
    /*switch (set.mode) { 
      case 0: break;
      case 1: set.numwaves = 1; break; 
      case 2: break;
      case 3: break;
      case 4: set.numwaves = 1; break;
      case 5: set.numwaves = 1; break;
      case 6: set.numwaves = 1; break;
    }*/
  }
  
  switch(s){
    case 0: set.mode = 0; distance=1; set.numwaves=1; reflection2parametre=1;                               break; // static
    case 1: set.mode = 1; distance=255;  set.numwaves=NUMPIXELS; reflection2parametre=1;                    break; // breathing
    case 2: set.mode = 2; distance=255; reflection2parametre=1; selectionDir(set.dir, NULL);                break; // cycle
    case 3: set.mode = 3; distance=97; reflection2parametre=1; selectionDir(set.dir, NULL);                 break; // reflection
    case 4: set.mode = 4; distance=97; reflection2parametre=2; set.numwaves=2; selectionDir(set.dir, NULL); break; // resistance
    case 5: set.mode = 5; distance=255; set.numwaves=2; autoLowPass();                                      break; // music show
    case 6: set.mode = 6; distance=255; set.numwaves=2; autoLowPass();                                      break; // inverted music show
    case modeOptionsAmount-1: (*in) = '0';                                                                  break; // exit
  }
  #if (DEV_LOG == 1) 
    Serial.print(F("current mode is:     ")); Serial.println(set.mode);
    Serial.print(F("current numwaves is: ")); Serial.println(set.numwaves);
    Serial.print(F("current distance is: ")); Serial.println(distance);
  #endif
}
