// buttons / nullButtonFlags / buttonCheck
void buttons(void) {
  if (button1S && !button1F && millis() - clickTime > debounce) {
    button1F = 1;
    clickTime = millis();
  }
  // если отпустили до hold, считать отпущенной
  if (!button1S && button1F && !button1R && !button1DP && millis() - clickTime < hold) {
    button1R = 1;
    button1F = 0;
    clickDoubleTime = millis();
  }
  // если отпустили и прошло больше doubleClickDebounce, считать 1 нажатием
  if (button1R && !button1DP && millis() - clickDoubleTime > doubleClickDebounce) {
    button1R = 0;
    button1P = 1;
  }
  // если отпустили и прошло меньше doubleClickDebounce и нажата снова, считать что нажата 2 раз
  if (button1F && !button1DP && button1R && millis() - clickDoubleTime < doubleClickDebounce) {
    button1F = 0;
    button1R = 0;
    button1DP = 1;
  }
  // если была нажата 2 раз и отпущена, считать что была нажата 2 раза
  if (button1DP && millis() - clickTime < hold) {
    button1DP = 0;
    button1D = 1;
    clickDoubleTime = millis();
  }
  // то же для второй кнопки
  if (button2S && !button2F && millis() - clickTime > debounce) {
    button2F = 1;
    clickTime = millis();
  }
  // если отпустили до hold, считать отпущенной
  if (!button2S && button2F && !button2R && !button2DP && millis() - clickTime < hold) {
    button2R = 1;
    button2F = 0;
    clickDoubleTime = millis();
  }
  // если отпустили и прошло больше doubleClickDebounce, считать 1 нажатием
  if (button2R && !button2DP && millis() - clickDoubleTime > doubleClickDebounce) {
    button2R = 0;
    button2P = 1;
  }
  // если отпустили и прошло меньше doubleClickDebounce и нажата снова, считать что нажата 2 раз
  if (button2F && !button2DP && button2R && millis() - clickDoubleTime < doubleClickDebounce) {
    button2F = 0;
    button2R = 0;
    button2DP = 1;
  }
  // если была нажата 2 раз и отпущена, считать что была нажата 2 раза
  if (button2DP && millis() - clickTime < hold) {
    button2DP = 0;
    button2D = 1;
    clickDoubleTime = millis();
  }

  // то же для R кнопки
  if (buttonRS && !buttonRF && millis() - clickTime > debounce) {
    buttonRF = 1;
    clickTime = millis();
  }
  // если отпустили до hold, считать отпущенной
  if (!buttonRS && buttonRF && !buttonRR && !buttonRDP && millis() - clickTime < hold) {
    buttonRR = 1;
    buttonRF = 0;
    clickDoubleTime = millis();
  }
  // если отпустили и прошло больше doubleClickDebounce, считать 1 нажатием
  if (buttonRR && !buttonRDP && millis() - clickDoubleTime > doubleClickDebounce) {
    buttonRR = 0;
    buttonRP = 1;
  }
  // если отпустили и прошло меньше doubleClickDebounce и нажата снова, считать что нажата 2 раз
  if (buttonRF && !buttonRDP && buttonRR && millis() - clickDoubleTime < doubleClickDebounce) {
    buttonRF = 0;
    buttonRR = 0;
    buttonRDP = 1;
  }
  // если была нажата 2 раз и отпущена, считать что была нажата 2 раза
  if (buttonRDP && millis() - clickTime < hold) {
    buttonRDP = 0;
    buttonRD = 1;
    clickDoubleTime = millis();
  }
}


void nullButtonFlags(void) {
    button1S = 0; button1F = 0; button1P = 0; button1R = 0; button1D = 0; button1DP = 0;
    button2S = 0; button2F = 0; button2P = 0; button2R = 0; button2D = 0; button2DP = 0;
    buttonRS = 0; buttonRF = 0; buttonRP = 0; buttonRR = 0; buttonRD = 0; buttonRDP = 0;
    newInput = 0;
}

   
void buttonCheck(char& sym) {
    button1S=digitalRead( buttonPin1 );
    button2S=digitalRead( buttonPin2 );  
    buttonRS=digitalRead( buttonPinR ); 
    buttons(); 
    if (button1P) { button1P = 0; newInput=1; sym='1'; }
    if (button2P) { button2P = 0; newInput=1; sym='2'; }
    if (buttonRP) { buttonRP = 0; newInput=1; sym='R'; }
    if (button1D || button2D) {button1D = 0; newInput=1; sym='3'; }
    #if (DEV_LOG == 1) 
    if (Serial.available()) {sym = Serial.read(); newInput = 1;}
    #endif
}
